import CartItem from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with props',
    props: {
      imageUrl: '//',
      price: 99.99,
      quantity: 10,
      titile: 'Amazing title',
    },
  },
  {
    name: 'with quantity at zero',
    props: {
      imageUrl: '//',
      price: 99.99,
      quantity: 0,
      titile: 'Amazing title',
    },
  },
];

itRendersAllMutations(CartItem, mutations);
