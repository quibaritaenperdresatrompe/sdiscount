import React, { Component } from 'react';
import { number, string } from 'prop-types';

import './styles.css';

class CartItem extends Component {
  static defaultProps = {
    imageUrl: null,
    price: 0,
    quantity: 0,
    title: '',
  };

  static propTypes = {
    imageUrl: string,
    price: number,
    quantity: number,
    title: string,
  };

  render() {
    const { imageUrl, price, quantity, title } = this.props;

    if (quantity === 0) return null;

    return (
      <li className="CartItem">
        <div className="CartItem-detail CartItem-detail-imageContainer">
          <img
            className="CartItem-detail-image"
            src={imageUrl}
            alt={title}
          />
        </div>
        <div className="CartItem-detail CartItem-detail-title">
          {title}
        </div>
        <div className="CartItem-detail CartItem-detail-number">
          {price.toFixed(2)} €
        </div>
        <div className="CartItem-detail CartItem-detail-number">
          {quantity}
        </div>
        <div className="CartItem-detail CartItem-detail-number">
          <strong>{(price * quantity).toFixed(2)} €</strong>
        </div>
      </li>
    );
  }
}

export const cartItemTypes = CartItem.propTypes;
export default CartItem;
