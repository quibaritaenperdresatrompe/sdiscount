import Product from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with price and title',
    props: {
      price: 9.99,
      title: 'Amazing title',
    },
  },
  {
    name: 'with all props',
    props: {
      addToCart: jest.fn(),
      description: 'Wonderful description',
      imageUrl: 'http://img.com',
      price: 9.99,
      reference: '000',
      size: 100,
      title: 'Amazing title',
    },
  },
];

itRendersAllMutations(Product, mutations);
