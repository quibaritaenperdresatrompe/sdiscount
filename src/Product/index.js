import { func, number, string } from 'prop-types';
import React, { Component } from 'react';
import styled from 'react-emotion';

import Button from '../Button';
import Image from '../Image';

const ProductContainer = styled('div')(
  {
    display: 'flex',
    flexDirection: 'column',
  },
  ({ size }) => ({
    width: size,
  }),
);

const SectionContainer = styled('div')({
  padding: '0 2em',
});

export const productTypes = {
  description: string,
  imageUrl: string,
  price: number.isRequired,
  reference: string.isRequired,
  title: string.isRequired,
};

class Product extends Component {
  static defaultProps = {
    addToCart: Function.prototype,
    description: '',
    imageUrl: null,
    size: null,
  };

  static propTypes = {
    addToCart: func,
    size: number,
  };

  handleAddToCart = () => {
    const { addToCart, reference } = this.props;

    addToCart(reference);
  };

  renderContent() {
    const { description, price, title } = this.props;

    return (
      <SectionContainer>
        <h2>{title}</h2>
        <p>
          <strong>{price.toFixed(2)}</strong> €
        </p>
        <p>{description}</p>
      </SectionContainer>
    );
  }

  renderAddToCartButton = () => {
    return <Button action={this.handleAddToCart} title="Ajouter" />;
  };

  renderActions() {
    return (
      <SectionContainer>
        {this.renderAddToCartButton()}
      </SectionContainer>
    );
  }

  render() {
    const { imageUrl, price, title, size } = this.props;

    if (!price || !title) return null;

    return (
      <ProductContainer size={size}>
        <Image title={title} url={imageUrl} size={size} />
        {this.renderContent()}
        {this.renderActions()}
      </ProductContainer>
    );
  }
}

export default Product;
