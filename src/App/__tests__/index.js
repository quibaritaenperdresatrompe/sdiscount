import App from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

beforeEach(function() {
  window.fetch = jest
    .fn()
    .mockImplementation(() => new Promise(() => {}));
});

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with all props',
    props: {
      setProducts: jest.fn(),
    },
  },
];

itRendersAllMutations(App, mutations);
