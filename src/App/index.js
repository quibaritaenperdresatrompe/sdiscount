import { func } from 'prop-types';
import { BrowserRouter, Route } from 'react-router-dom';
import React, { Component } from 'react';

import config from '../config';
import Cart from '../ConnectedCart';
import Header from '../ConnectedHeader';
import ProductList from '../ConnectedProductList';

class App extends Component {
  static defaultProps = {
    setProducts: Function.prototype,
  };

  static propTypes = {
    setProducts: func,
  };

  componentDidMount() {
    const { setProducts } = this.props;

    fetch(`${config.apiBaseUrl}/products`)
      .then(response => response.json())
      .then(data => {
        setProducts(data);
      });
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Header title="Sdiscount" />
          <Route exact path="/" component={ProductList} />
          <Route exact path="/cart" component={Cart} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
