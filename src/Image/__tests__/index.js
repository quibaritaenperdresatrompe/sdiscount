import Image from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'without url',
    props: {
      title: 'Amazing title',
      size: 100,
    },
  },
  {
    name: 'without all props',
    props: {
      title: 'Amazing title',
      url: '//',
      size: 100,
    },
  },
];

itRendersAllMutations(Image, mutations);
