import React from 'react';
import styled from 'react-emotion';
import { number, string } from 'prop-types';

const ImageContainer = styled('div')(
  {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  ({ size }) => ({
    height: size,
  }),
);

const StyledImage = styled('img')(({ size }) => ({
  width: size,
  maxHeight: size,
}));

const Image = ({ title, url, size }) => {
  if (!url) return null;

  return (
    <ImageContainer size={size}>
      <StyledImage size={size} src={url} alt={title} />
    </ImageContainer>
  );
};

Image.defaultProps = {
  size: 90,
  title: '',
  url: null,
};

Image.propTypes = {
  size: number,
  title: string,
  url: string,
};

export default Image;
