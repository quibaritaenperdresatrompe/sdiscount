import { connect } from 'react-redux';

import { emptyCart } from '../ConnectedProductList/redux/actions';
import Cart from '../Cart';

const mapStateToProps = state => ({
  items: state.productListReducer.products.filter(
    product => product.quantity > 0,
  ),
});

const mapDispatchToProps = dispatch => ({
  emptyCart: () => dispatch(emptyCart()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Cart);
