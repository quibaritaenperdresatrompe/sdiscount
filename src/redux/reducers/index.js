import { combineReducers } from 'redux';

import productListReducer from '../../ConnectedProductList/redux/reducers';

export default combineReducers({
  productListReducer,
});
