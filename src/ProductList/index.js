import { arrayOf, shape } from 'prop-types';
import React, { Component } from 'react';
import styled from 'react-emotion';

import { productTypes } from '../Product';
import Product from '../ConnectedProduct';

const ProductListContainer = styled('div')({
  display: 'flex',
  flexWrap: 'wrap',
});

class ProductList extends Component {
  static defaultProps = {
    products: [],
  };

  static propTypes = {
    products: arrayOf(shape(productTypes)),
  };

  renderProduct = product => {
    const { reference } = product;

    return <Product {...product} size={300} key={reference} />;
  };

  renderProducts = () => {
    const { products } = this.props;

    if (products.length === 0) return 'Pas de produit disponible';

    return products.map(this.renderProduct);
  };

  render() {
    return (
      <ProductListContainer>
        {this.renderProducts()}
      </ProductListContainer>
    );
  }
}

export default ProductList;
