import ProductList from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

beforeEach(function() {
  window.fetch = jest
    .fn()
    .mockImplementation(() => new Promise(() => {}));
});

const mockedProduct = {
  imageUrl: '//',
  title: 'Amazing title',
  description: 'Wonderful description',
  price: 999.9,
  reference: '0000',
};

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with one product',
    props: {},
  },
  {
    name: 'with more products',
    props: {
      products: [mockedProduct, mockedProduct],
      setProducts: jest.fn(),
    },
  },
];

itRendersAllMutations(ProductList, mutations);
