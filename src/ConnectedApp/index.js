import { connect } from 'react-redux';

import { setProducts } from '../ConnectedProductList/redux/actions';
import App from '../App';

const mapDispatchToProps = dispatch => ({
  setProducts: products => dispatch(setProducts(products)),
});

export default connect(
  null,
  mapDispatchToProps,
)(App);
