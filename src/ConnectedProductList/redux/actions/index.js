import {
  ADD_PRODUCT_TO_CART,
  EMPTY_CART,
  SET_PRODUCTS,
} from '../actionTypes';

export const emptyCart = () => ({
  type: EMPTY_CART,
});

export const addProductToCart = productRef => ({
  type: ADD_PRODUCT_TO_CART,
  payload: {
    productRef,
  },
});

export const setProducts = products => ({
  type: SET_PRODUCTS,
  payload: {
    products,
  },
});
