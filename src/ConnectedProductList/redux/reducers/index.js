import {
  ADD_PRODUCT_TO_CART,
  EMPTY_CART,
  SET_PRODUCTS,
} from '../actionTypes';

const intialState = {
  products: [],
};

const setProducts = (prevState, products) => ({
  ...prevState,
  products: products.map(product => ({ ...product, quantity: 0 })),
});

const incrementProductQuantity = (prevState, productRef) => ({
  ...prevState,
  products: prevState.products.map(product => {
    if (product.reference === productRef) {
      return {
        ...product,
        quantity: product.quantity + 1,
      };
    }

    return product;
  }),
});

const resetProductQuantities = prevState => ({
  ...prevState,
  products: prevState.products.map(product => ({
    ...product,
    quantity: 0,
  })),
});

export default (state = intialState, { type, payload }) => {
  switch (type) {
    case SET_PRODUCTS:
      return setProducts(state, payload.products);
    case ADD_PRODUCT_TO_CART:
      return incrementProductQuantity(state, payload.productRef);
    case EMPTY_CART:
      return resetProductQuantities(state);
    default:
      return state;
  }
};
