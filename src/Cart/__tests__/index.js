import Cart from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

const mockedItem = {
  imageUrl: '//',
  price: 9.99,
  quantity: 1,
  title: 'Amazing title',
  reference: '0001',
};

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with one item',
    props: {
      items: [mockedItem],
    },
  },
  {
    name: 'with more items',
    props: {
      items: [mockedItem, mockedItem],
    },
  },
];

itRendersAllMutations(Cart, mutations);
