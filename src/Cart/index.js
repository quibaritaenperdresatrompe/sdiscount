import React, { Component } from 'react';
import { arrayOf, func, shape, string } from 'prop-types';

import Button from '../Button';
import CartItem, { cartItemTypes } from '../CartItem';
import styled from 'react-emotion';

const CartContainer = styled('ol')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-around',
  listStyle: 'none',
  margin: 0,
  padding: '0 2em',
});

const StyledTotals = styled('li')({
  marginBottom: '2em',
  marginTop: '2em',
});

class Cart extends Component {
  constructor() {
    super();
    this.state = {
      isPaid: false,
    };
  }

  static defaultProps = {
    emptyCart: Function.prototype,
    items: [],
  };

  static propTypes = {
    emptyCart: func,
    items: arrayOf(shape({ ...cartItemTypes, reference: string })),
  };

  handlePayButtonClick = () => {
    const { emptyCart } = this.props;
    const delayBeforeRedirection = 2000;

    this.setState(
      () => ({
        isPaid: true,
      }),
      () => setTimeout(emptyCart, delayBeforeRedirection),
    );
  };

  renderItems = () => {
    const { items } = this.props;

    return items.map(({ reference, ...otherProps }) => (
      <CartItem key={reference} {...otherProps} />
    ));
  };

  renderTotals = () => {
    const { items } = this.props;

    const { totalCount, totalPrice } = items.reduce(
      ({ totalCount, totalPrice }, { price, quantity }) => ({
        totalCount: totalCount + quantity,
        totalPrice: totalPrice + price * quantity,
      }),
      { totalCount: 0, totalPrice: 0 },
    );

    return (
      <StyledTotals key="totals">
        <span>
          <strong>{totalCount}</strong> produit(s) pour un total de{' '}
          <strong>{totalPrice.toFixed(2)}</strong> €
        </span>
      </StyledTotals>
    );
  };

  renderPayButton = () => {
    const { isPaid } = this.state;

    const title = isPaid ? 'Commande validée et payée !' : 'Payer';

    return (
      <Button
        action={this.handlePayButtonClick}
        title={title}
        disabled={isPaid}
      />
    );
  };

  render() {
    const { items } = this.props;

    if (items.length === 0)
      return (
        <CartContainer>
          <h2>Aucun produit ajouté au panier</h2>
        </CartContainer>
      );

    return (
      <CartContainer>
        {this.renderItems()}
        {this.renderTotals()}
        {this.renderPayButton()}
      </CartContainer>
    );
  }
}

export default Cart;
