import { connect } from 'react-redux';

import { addProductToCart } from '../ConnectedProductList/redux/actions';
import Product from '../Product';

const mapDispatchToProps = dispatch => ({
  addToCart: productRef => dispatch(addProductToCart(productRef)),
});

export default connect(
  null,
  mapDispatchToProps,
)(Product);
