import Button from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with all props',
    props: {
      action: jest.fn(),
      title: 'Amazing title',
    },
  },
  {
    name: 'disabled with all props',
    props: {
      action: jest.fn(),
      disabled: true,
      title: 'Amazing title',
    },
  },
];

itRendersAllMutations(Button, mutations);
