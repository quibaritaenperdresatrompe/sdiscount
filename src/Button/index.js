import React from 'react';
import styled from 'react-emotion';
import { bool, func, string } from 'prop-types';

const StyledButton = styled('button')({
  border: 0,
  backgroundColor: 'grey',
  color: 'white',
  padding: '0.5em 1em',
  fontWeight: 700,
  cursor: 'pointer',
  ':hover': {
    backgroundColor: 'black',
  },
});

const Button = ({ action, disabled, title }) => (
  <StyledButton onClick={action} disabled={disabled}>
    {title}
  </StyledButton>
);

Button.defaultProps = {
  action: Function.prototype,
  disabled: false,
  title: '',
};

Button.propTypes = {
  action: func,
  disabled: bool,
  title: string,
};

export default Button;
