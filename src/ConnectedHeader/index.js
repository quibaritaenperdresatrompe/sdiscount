import { connect } from 'react-redux';

import Header from '../Header';

const mapStateToProps = state => ({
  productCount: state.productListReducer.products.reduce(
    (sum, product) => sum + product.quantity,
    0,
  ),
});

export default connect(mapStateToProps)(Header);
