import { Link } from 'react-router-dom';
import { number, string } from 'prop-types';
import React, { Component } from 'react';

import './styles.css';

class Header extends Component {
  static defaultPros = {
    productCount: 0,
    title: null,
  };

  static propTypes = {
    productCount: number,
    title: string,
  };

  renderTitle = () => {
    const { title } = this.props;

    if (!title) return null;

    return (
      <Link to="/" className="Header-link">
        <h1 className="Header-title">
          <span
            className="Header-title-emoji"
            role="img"
            aria-label="sushi"
          >
            🍣
          </span>
          {title}
        </h1>
      </Link>
    );
  };

  renderProductCount = () => {
    const { productCount } = this.props;
    if (productCount < 1) return null;
    return (
      <span className="Header-productCount">{`${productCount} produit(s)`}</span>
    );
  };

  renderCartLink = () => (
    <Link to="/cart" className="Header-link Header-navButton ">
      Mon panier
      {this.renderProductCount()}
    </Link>
  );

  render() {
    return (
      <header className="Header">
        {this.renderTitle()}
        {this.renderCartLink()}
      </header>
    );
  }
}

export default Header;
