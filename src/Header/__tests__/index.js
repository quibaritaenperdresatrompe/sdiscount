import Header from '../';
import itRendersAllMutations from '../../../lib/it-renders-all-mutations';

const mutations = [
  {
    name: 'without props',
    props: {},
  },
  {
    name: 'with title',
    props: {
      title: 'Amazing title',
    },
  },
  {
    name: 'with productCount',
    props: {
      productCount: 99,
    },
  },
];

itRendersAllMutations(Header, mutations);
