# Sdiscount 🍣

Sushi e-shop web app built with [React](https://github.com/facebook/react).<br/>
You can test the [demo version](https://confident-williams-7d37ff.netlify.com/) online.

It uses a mocked REST API built with [json-server](https://github.com/typicode/json-server) and [deployed](https://github.com/jesperorb/json-server-heroku#deploy-to-heroku) with [Heroku](https://dashboard.heroku.com) at [https://sdiscount-api.herokuapp.com/](https://sdiscount-api.herokuapp.com/).

## How to run

After having cloned the sdiscount repository, run the following commands at the sdiscount root: `yarn`.

## Available scripts

### `yarn start`

Runs the app in the development mode.<br/>
It will automatically open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br/>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.<br/>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br/>
Your app is ready to be deployed!

## Continuous delivery

This app is automatically deployed with [Netlify](https://app.netlify.com/).
