import itRendersCorrectly from '../it-renders-correctly';

const itRendersAllMutations = (component, mutations) => {
  describe(`${component.name}`, () => {
    describe('it renders all mutations', () => {
      mutations.forEach(({ name, props, state }) =>
        itRendersCorrectly(component, name, props, state),
      );
    });
  });
};

export default itRendersAllMutations;
