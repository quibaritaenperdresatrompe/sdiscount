import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import toJSON from 'enzyme-to-json';

configure({ adapter: new Adapter() });

const itRendersCorrectly = (Component, name, props, state) => {
  test(`it renders correctly ${name}`, () => {
    const component = shallow(<Component {...props} />);

    if (state) component.setState(state);

    const tree = toJSON(component);

    expect(tree).toMatchSnapshot();
  });
};

export default itRendersCorrectly;
